use super::model::Model;
use std::io::Result;

use tui::backend::Backend;
use tui::layout::{Constraint, Direction, Layout, Rect};
use tui::style::{Color, Modifier, Style};
use tui::terminal::Frame;
use tui::text::{Span, Spans, Text};
use tui::widgets::{Block, Borders, Gauge, List, ListItem, Paragraph};
use tui::Terminal;

use srs::message::{Modulation, Radio};

use crate::channel_logger::LogMsg;
use crate::state::State;

pub fn modulation_str(m: &Modulation) -> &str {
    match m {
        Modulation::AM => "AM",
        Modulation::FM => "FM",
        Modulation::Intercom => "Intercom",
        Modulation::Disabled => "Disabled",
        Modulation::HaveQuick => "HaveQuick",
        Modulation::Satcom => "Satcom",
        Modulation::Mids => "Mids",
    }
}

pub fn draw_radio(f: &mut Frame<impl Backend>, area: Rect, radio: &Radio, is_active: bool) {
    let border_color = if is_active {
        Color::LightGreen
    } else {
        Color::Gray
    };

    let block = Block::default()
        .title(radio.name.clone())
        .borders(Borders::TOP | Borders::LEFT)
        .border_style(Style::default().fg(border_color))
        .style(Style::default());

    let freq_string = format!("{} MHz", radio.freq / 1_000_000.);

    if let Modulation::Disabled = radio.modulation {
        f.render_widget(
            Paragraph::new(Text::styled("disabled", Style::default().fg(Color::Gray))),
            block.inner(area),
        )
    } else {
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints(vec![
                Constraint::Length(1),
                Constraint::Length(1),
                Constraint::Length(1),
            ])
            .split(block.inner(area));

        let text = vec![Spans::from(vec![
            Span::raw(" "),
            Span::styled(freq_string, Style::default()),
            Span::raw(" "),
            Span::styled(
                modulation_str(&radio.modulation),
                Style::default().fg(Color::Gray),
            ),
        ])];

        f.render_widget(Paragraph::new(text), chunks[0]);

        let volume = Gauge::default()
            .gauge_style(
                Style::default()
                    .fg(Color::Gray)
                    .add_modifier(Modifier::ITALIC),
            )
            .ratio(radio.volume as f64);

        f.render_widget(volume, chunks[1])
    }
    f.render_widget(block, area);
}

pub fn draw_game_state(f: &mut Frame<impl Backend>, area: Rect, state: State) {
    if state.radios.is_empty() {
        let w = Paragraph::new(Text::styled("No radios", Style::default()));
        f.render_widget(w, area)
    } else {
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints(
                state
                    .radios
                    .iter()
                    .map(|_| Constraint::Length(5))
                    .collect::<Vec<_>>(),
            )
            .split(f.size());

        let selected = state.selected_radio().map(|(i, _radio)| i);
        for (i, (r, chunk)) in state.radios.iter().zip(chunks).enumerate() {
            draw_radio(f, chunk, r, Some(i) == selected)
        }
    }
}

pub fn log_msg_item(msg: &LogMsg) -> ListItem {
    let (color, text) = match msg.0 {
        flexi_logger::Level::Error => (Color::Red, "error"),
        flexi_logger::Level::Warn => (Color::Blue, "warn"),
        flexi_logger::Level::Info => (Color::Magenta, "info"),
        flexi_logger::Level::Debug => (Color::Gray, "debug"),
        flexi_logger::Level::Trace => (Color::DarkGray, "trace"),
    };

    ListItem::new(Spans::from(vec![
        Span::raw("["),
        Span::styled(text, Style::default().fg(color)),
        Span::raw("] "),
        Span::raw(&msg.1),
    ]))
}

// Draw the whole TUI.
pub async fn draw(terminal: &mut Terminal<impl Backend>, model: &Model) -> Result<()> {
    let state = model.srs_state.lock().await.state().clone();
    terminal.draw(|f| {
        let msgs_to_draw = &model.log_msgs.iter().rev().take(10).collect::<Vec<_>>();

        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints(
                [
                    Constraint::Min(1),
                    Constraint::Length(msgs_to_draw.len() as u16),
                ]
                .as_ref(),
            )
            .split(f.size());

        let w = List::new(
            msgs_to_draw
                .iter()
                .cloned()
                .map(log_msg_item)
                .collect::<Vec<_>>(),
        );

        draw_game_state(f, chunks[0], state);
        f.render_widget(w, chunks[1]);
    })
}
