// use std::io;
use std::sync::Arc;

use tokio::sync::Mutex;

use crate::channel_logger::LogMsg;
use crate::state::StateManager;

pub struct Model {
    pub log_msgs: Vec<LogMsg>,
    pub srs_state: Arc<Mutex<StateManager>>,
}

impl Model {
    pub fn new(srs_state: Arc<Mutex<StateManager>>) -> Self {
        Self {
            log_msgs: vec![],
            srs_state,
        }
    }

    pub async fn set_ptt(self, to: bool) -> Self {
        self.srs_state.lock().await.set_ptt(to);
        self
    }

    pub async fn prev_radio(self) -> Self {
        self.srs_state.lock().await.prev_radio();
        self
    }
    pub async fn next_radio(self) -> Self {
        self.srs_state.lock().await.next_radio();
        self
    }

    pub async fn radio_add_mhz(self, amount: f64) -> Self {
        self.srs_state.lock().await.radio_add_mhz(amount);
        self
    }
}
