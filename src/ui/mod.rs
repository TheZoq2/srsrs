use std::io;
use std::sync::{mpsc::Receiver, Arc, Mutex as StdMutex};

use tokio::sync::oneshot::Sender;
use tokio::sync::Mutex as TokioMutex;

use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use tui::backend::TermionBackend;
use tui::Terminal;

use birch::event::{Event, Events};

pub mod input;
mod model;
mod msg;
mod view;

use crate::channel_logger::LogMsg;
use crate::state::StateManager;
use model::Model;
use msg::{Cmd, Msg};

use anyhow::Result;

macro_rules! loopback {
    ($msg:expr) => {
        vec![Cmd::Loopback($msg)]
    };
}

async fn update(msg: Msg, mut model: Model) -> (Model, Vec<Cmd>) {
    match msg {
        Msg::KeyInput(key) => {
            // Check if this is a global command
            if key == Key::Ctrl('c') || key == Key::Ctrl('d') {
                (model, loopback!(Msg::Exit))
            } else {
                (model, vec![])
            }
        }
        Msg::NewLogMsg(msg) => {
            model.log_msgs.push(msg);
            (model, vec![])
        }
        Msg::PttOn => {
            info!("Ptt on");
            (model.set_ptt(true).await, vec![])
        }
        Msg::PttOff => (model.set_ptt(false).await, vec![]),
        Msg::NextRadio => (model.next_radio().await, vec![]),
        Msg::PrevRadio => (model.prev_radio().await, vec![]),
        Msg::RadioAdd01MHz => (model.radio_add_mhz(0.1).await, vec![]),
        Msg::RadioAdd1MHz => (model.radio_add_mhz(1.0).await, vec![]),
        Msg::RadioAdd10MHz => (model.radio_add_mhz(10.0).await, vec![]),
        Msg::RadioSub01MHz => (model.radio_add_mhz(-0.1).await, vec![]),
        Msg::RadioSub1MHz => (model.radio_add_mhz(-1.0).await, vec![]),
        Msg::RadioSub10MHz => (model.radio_add_mhz(-10.0).await, vec![]),
        Msg::Exit => panic!("Got exit message inside update function"),
    }
}

pub async fn run_ui(
    state: Arc<TokioMutex<StateManager>>,
    shutdown_tx: Sender<()>,
    log_msg_receiver: Receiver<LogMsg>,
) -> Result<()> {
    // Set up stdin
    // let stdin_keys = Arc::new(Mutex::new(io::stdin().keys()));

    // Set up the model
    let mut model = Model::new(state);

    // Set up termion
    let stdout = io::stdout().into_raw_mode()?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    terminal.clear()?;

    let stdin_keys = Arc::new(StdMutex::new(io::stdin().keys()));
    let events = Events::new(stdin_keys);

    let bindings = input::Bindings::from_file()?;
    let mut gamepad_reader = input::GilrsManager::new(&bindings)?;
    let mut x11reader = input::X11Manager::new(bindings);

    // Messages received in this iteration. Add messages that should be
    // send in the first iteration here.
    let mut msgs = vec![];
    'main: loop {
        // Check input
        #[allow(clippy::single_match)]
        match events.try_next() {
            Ok(Event::Input(key)) => {
                msgs.push(Msg::KeyInput(key));
            }
            Ok(_) => {}
            Err(std::sync::mpsc::TryRecvError::Empty) => {}
            Err(_) => break 'main Ok(()),
        }
        tokio::task::yield_now().await;

        // Check for log messages
        while let Ok(log_msg) = log_msg_receiver.try_recv() {
            msgs.push(Msg::NewLogMsg(log_msg))
        }

        tokio::task::yield_now().await;
        // Check for input
        for msg in gamepad_reader.poll() {
            msgs.push(msg)
        }
        // Check for input
        for msg in x11reader.poll() {
            msgs.push(msg)
        }

        tokio::task::yield_now().await;
        // Update the model with all new messages
        while let Some(msg) = msgs.pop() {
            // Exit messages have to be handled here, otherwise the terminal
            // does not get cleaned up
            if let Msg::Exit = msg {
                shutdown_tx.send(()).unwrap();
                break 'main Ok(());
            }
            let (new_model, new_cmds) = update(msg, model).await;
            model = new_model;
            // Run commands
            for cmd in new_cmds {
                match cmd {
                    Cmd::Loopback(msg) => msgs.push(msg),
                }
            }
        }

        view::draw(&mut terminal, &model).await?;
        tokio::task::yield_now().await;
    }
}
