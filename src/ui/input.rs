use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::PathBuf;

use anyhow::{Context, Result};

use gilrs::{Event, EventType, Gilrs};

use serde::{Deserialize, Serialize};

use x11rb::connection::Connection;
use x11rb::protocol::xproto::*;
use x11rb::protocol::Event as X11Event;
use x11rb::rust_connection::DefaultStream;
use x11rb::rust_connection::RustConnection;

use super::msg::Msg;

const MODIFIER_KEY_MASK: u16 = KeyButMask::Shift as u16
    | KeyButMask::Lock as u16
    | KeyButMask::Control as u16
    | KeyButMask::Mod1 as u16
    | KeyButMask::Mod2 as u16
    | KeyButMask::Mod3 as u16
    | KeyButMask::Mod4 as u16
    | KeyButMask::Mod5 as u16;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("No binding config file exists")]
    NoConfigFile,
    // This is a string because anyhow doesn't like gilrs errors aparently
    #[error("GILRS error: {0}")]
    GilrsError(String),
    #[error("IO error {0}")]
    Io(#[from] std::io::Error),
    #[error("Toml deserialization error {0}")]
    Json(#[from] serde_json::Error),
    #[error("Failed to connect to x server: {0}")]
    X11ConnectError(#[from] x11rb::errors::ConnectError),
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Key {
    // NOTE: We may want to use uuid here
    Gamepad(String, u32),
    // x11 modifier state, X11 keycode
    Keyboard(u8, u16),
}

impl Key {
    pub fn check_gilrs(&self, gamepad: &str, code: &gilrs::ev::Code) -> bool {
        match self {
            Key::Gamepad(g, c) if g == &gamepad && *c == code.into_u32() => true,
            _ => false,
        }
    }
    #[allow(dead_code)]
    pub fn check_x11(&self, key: u8, status: u16) -> bool {
        match self {
            Key::Keyboard(k, s) if k == &key && *s == status & MODIFIER_KEY_MASK => true,
            _ => false,
        }
    }
}

#[derive(Clone, Debug)]
pub enum InputEvent {
    Press(Key),
    Release(Key),
}

#[derive(Clone, Default, Serialize, Deserialize)]
pub struct Bindings {
    pub ptt: Option<Key>,
    pub next_radio: Option<Key>,
    pub prev_radio: Option<Key>,
    pub radio_add_01mhz: Option<Key>,
    pub radio_add_1mhz: Option<Key>,
    pub radio_add_10mhz: Option<Key>,
    pub radio_sub_01mhz: Option<Key>,
    pub radio_sub_1mhz: Option<Key>,
    pub radio_sub_10mhz: Option<Key>,
    // NOTE: When adding more members, remember to register them in Bindings
}

impl Bindings {
    pub fn filename() -> PathBuf {
        "srsrs.json".into()
    }
    pub fn from_file() -> std::result::Result<Self, Error> {
        if !Self::filename().exists() {
            Err(Error::NoConfigFile)
        } else {
            let mut file = File::open(&Self::filename())?;
            let mut content = String::new();
            file.read_to_string(&mut content)?;

            Ok(serde_json::from_str::<Self>(&content)?)
        }
    }
    pub fn save(&self) -> std::result::Result<(), Error> {
        let content = serde_json::to_string(self)?;
        let mut file = File::create(&Self::filename())?;
        file.write_all(&mut content.as_bytes())?;
        Ok(())
    }
    pub fn listeners(&self) -> Vec<(InputEvent, Msg)> {
        let mut result = vec![];
        macro_rules! reg_event {
            ($member:ident, $on_down:expr$(, $on_up:expr)? ) => {
                if let Some(k) = self.$member.clone() {
                    $( result.push((InputEvent::Release(k.clone()), $on_up)); )?
                    result.push((InputEvent::Press(k), $on_down));
                }
            }
        }
        reg_event!(ptt, Msg::PttOn, Msg::PttOff);
        reg_event!(next_radio, Msg::NextRadio);
        reg_event!(prev_radio, Msg::PrevRadio);
        reg_event!(radio_add_01mhz, Msg::RadioAdd01MHz);
        reg_event!(radio_add_1mhz, Msg::RadioAdd1MHz);
        reg_event!(radio_add_10mhz, Msg::RadioAdd10MHz);
        reg_event!(radio_sub_01mhz, Msg::RadioSub01MHz);
        reg_event!(radio_sub_1mhz, Msg::RadioSub1MHz);
        reg_event!(radio_sub_10mhz, Msg::RadioSub10MHz);
        result
    }
}

pub struct GilrsManager {
    gilrs: Gilrs,
    listeners: Vec<(InputEvent, Msg)>,
}

impl GilrsManager {
    pub fn new(bindings: &Bindings) -> Result<Self> {
        let gilrs = Gilrs::new()
            .map_err(|e| Error::GilrsError(format!("{}", e)))
            .context("Failed to initialise gilrs")?;
        Ok(Self {
            gilrs,
            listeners: bindings.listeners(),
        })
    }

    pub fn poll(&mut self) -> Vec<Msg> {
        let mut result = vec![];
        while let Some(Event { id, event, .. }) = &self.gilrs.next_event() {
            if let EventType::ButtonPressed(_, code) = event {
                let gamepad = self.gilrs.gamepad(id.clone()).name().to_string();

                for (kind, msg) in &self.listeners {
                    if let InputEvent::Press(k) = kind {
                        if k.check_gilrs(&gamepad, code) {
                            result.push(msg.clone())
                        }
                    }
                }
            }
            if let EventType::ButtonReleased(_, code) = event {
                let gamepad = self.gilrs.gamepad(id.clone()).name().to_string();

                for (kind, msg) in &self.listeners {
                    if let InputEvent::Release(k) = kind {
                        if k.check_gilrs(&gamepad, code) {
                            result.push(msg.clone())
                        }
                    }
                }
            }
        }
        result
    }
}

pub struct X11Manager {
    listeners: Vec<(InputEvent, Msg)>,
    conn: RustConnection<DefaultStream>,
}

impl X11Manager {
    pub fn new(bindings: Bindings) -> Self {
        let (x11, screen_num) =
            RustConnection::connect(None).expect("failed to open x11 connection");
        let screen = &x11.setup().roots[screen_num];
        // Set up keygrabs for all events
        let listeners = bindings.listeners();
        for (evt, _) in listeners {
            match evt {
                InputEvent::Press(Key::Keyboard(code, mods))
                | InputEvent::Release(Key::Keyboard(code, mods)) => {
                    let c = x11
                        .grab_key(
                            false,
                            screen.root,
                            mods,
                            code,
                            GrabMode::Async,
                            GrabMode::Async,
                        )
                        .expect("failed to grab x11");

                    c.check().expect("failed to grab x11");
                    // .map_err(|e| info!("Failed to grab x11 {}", e))
                    // .and_then(|c| c.check().map_err(|e| info!("Failed to grab x11 {}", e)))
                    // .unwrap();

                    // println!("Listening for evt {:?}", evt);
                }
                _ => {}
            }
        }

        Self {
            listeners: bindings.listeners(),
            conn: x11,
        }
    }

    pub fn poll(&mut self) -> Vec<Msg> {
        let mut result = vec![];
        while let Some(event) = self
            .conn
            .poll_for_event()
            .expect("failed to poll for x11 events")
        {
            match event {
                X11Event::KeyPress(KeyPressEvent { detail, state, .. }) => {
                    for (kind, msg) in &self.listeners {
                        if let InputEvent::Press(k) = kind {
                            if k.check_x11(detail, state) {
                                result.push(msg.clone())
                            }
                        }
                    }
                }
                X11Event::KeyRelease(KeyReleaseEvent { detail, state, .. }) => {
                    for (kind, msg) in &self.listeners {
                        if let InputEvent::Release(k) = kind {
                            if k.check_x11(detail, state) {
                                result.push(msg.clone())
                            }
                        }
                    }
                }
                X11Event::Error(e) => {
                    println!("x11 error {:?}", e)
                }
                _ => {}
            }
        }
        result
    }
}

// Binding configuration

pub fn read_key(gilrs: &mut Gilrs, xconnection: &impl Connection) -> Result<Option<Key>> {
    // Clear the buffer of pre-existing events so we don't accidentally bind
    // a previous one
    while let Some(_) = gilrs.next_event() {}
    while let Some(_) = xconnection
        .poll_for_event()
        .context("failed to poll for x11 events")?
    {}

    loop {
        while let Some(Event { id, event, .. }) = gilrs.next_event() {
            match event {
                EventType::ButtonPressed(_, code) => {
                    println!("Got a keypress event");
                    let gamepad = gilrs.gamepad(id);
                    return Ok(Some(Key::Gamepad(gamepad.name().into(), code.into_u32())));
                }
                _ => {}
            }
        }

        while let Some(event) = xconnection
            .poll_for_event()
            .context("failed to poll for x11 events")?
        {
            match event {
                X11Event::KeyPress(KeyPressEvent { detail, state, .. }) => {
                    return Ok(Some(Key::Keyboard(detail, state & MODIFIER_KEY_MASK)))
                }
                _ => {}
            }
            println!("{:?}", event);
        }
    }
}

pub fn do_remapping() -> Result<()> {
    let mut gilrs = Gilrs::new()
        .map_err(|e| Error::GilrsError(format!("{}", e)))
        .context("Failed to initialise gilrs")?;

    // In order to receive keyboard input, we need a window. Let's create one
    let (conn, screen_num) = x11rb::connect(None)?;
    let screen = &conn.setup().roots[screen_num];
    let win_id = conn.generate_id()?;
    conn.create_window(
        x11rb::COPY_DEPTH_FROM_PARENT,
        win_id,
        screen.root,
        0,
        0,
        100,
        100,
        0,
        WindowClass::InputOutput,
        0,
        &CreateWindowAux::new()
            .event_mask(EventMask::KeyPress | EventMask::NoEvent)
            .background_pixel(screen.white_pixel),
    )?;
    // present::select_input(&conn, event_id, win_id, present::EventMask::)?;
    conn.map_window(win_id)?;
    conn.flush()?;

    // Try loading a config. If none exist, create a dummy config
    let mut config = match Bindings::from_file() {
        Ok(bindings) => Ok(bindings),
        Err(Error::NoConfigFile) => Ok(Bindings::default()),
        e @ Err(_) => e,
    }
    .context("Failed to load previous config file")?;

    macro_rules! binding {
        ($key:ident) => {
            config.$key = read_key(&mut gilrs, &conn)?
        };
    }

    loop {
        let mut input = String::new();

        println!("Which key would you like to change");
        println!("  1) ptt");
        println!("  2) next radio");
        println!("  3) prev radio");
        println!("  4) radio +0.1MHz");
        println!("  5) radio +1MHz");
        println!("  6) radio +10MHz");
        println!("  7) radio -0.1MHz");
        println!("  8) radio -1MHz");
        println!("  9) radio -10MHz");
        println!("  10) exit");
        match io::stdin().read_line(&mut input) {
            Ok(_n) => {
                match input.trim() {
                    "ptt" | "1" => binding!(ptt),
                    "next radio" | "2" => binding!(next_radio),
                    "prev radio" | "3" => binding!(prev_radio),
                    "radio +0.1MHz" | "4" => binding!(radio_add_01mhz),
                    "radio +1MHz" | "5" => binding!(radio_add_1mhz),
                    "radio +10MHz" | "6" => binding!(radio_add_10mhz),
                    "radio -0.1MHz" | "7" => binding!(radio_sub_01mhz),
                    "radio -1MHz" | "8" => binding!(radio_sub_1mhz),
                    "radio -10MHz" | "9" => binding!(radio_sub_10mhz),
                    "exit" | "10" => return Ok(()),
                    _ => {
                        println!("Unrecognised command");
                    }
                }

                config.save()?
            }
            Err(e) => return Err(Error::from(e).into()),
        }
    }
}
