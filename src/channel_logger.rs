use std::sync::mpsc::Sender;
use std::sync::Mutex;

use flexi_logger::{DeferredNow, Record};

#[derive(Clone, Debug)]
pub struct LogMsg(pub log::Level, pub String);

pub struct ChannelLogWriter {
    level: log::LevelFilter,
    tx: Mutex<Sender<LogMsg>>,
}

impl ChannelLogWriter {
    pub fn new(level: log::LevelFilter, tx: Sender<LogMsg>) -> Self {
        Self {
            level,
            tx: Mutex::new(tx),
        }
    }
}

impl flexi_logger::writers::LogWriter for ChannelLogWriter {
    fn write(&self, _: &mut DeferredNow, record: &Record) -> std::io::Result<()> {
        // If this send fails, we're already in deep shit, so just ignor the error (.ok())
        self.tx
            .lock()
            .unwrap()
            .send(LogMsg(record.level(), format!("{}", record.args())))
            .ok();
        Ok(())
    }

    fn flush(&self) -> std::io::Result<()> {
        println!("Flushing");
        Ok(())
    }

    fn max_log_level(&self) -> flexi_logger::LevelFilter {
        println!("max level: {:?}", self.level);
        self.level
    }
}
