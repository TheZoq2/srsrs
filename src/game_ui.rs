use std::{
    net::{IpAddr, Ipv4Addr, SocketAddr},
    sync::Arc,
};

use serde::{Deserialize, Serialize};
use srs::message::Radio;
use tokio::{net::UdpSocket, sync::Mutex};

use crate::state::{State, StateManager};

#[derive(Serialize, Deserialize, Debug)]
struct RadioInfo {
    #[serde(rename = "radios")]
    radios: Vec<Radio>,
    #[serde(rename = "selected")]
    selected: usize,
}

impl RadioInfo {
    pub fn from_state(state: &State) -> Self {
        Self {
            selected: state.radio_index,
            radios: state.radios.clone(),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct RadioState {
    #[serde(rename = "RadioInfo")]
    radio_info: RadioInfo,
}

impl RadioState {
    pub fn from_state(state: &State) -> Self {
        Self {
            radio_info: RadioInfo::from_state(state),
        }
    }
}

pub async fn run(state: Arc<Mutex<StateManager>>) {
    let mut update_receiver = state.lock().await.update_receiver.clone();

    // Connect to the game
    let mut socket = UdpSocket::bind("127.0.0.1:0").await.unwrap();
    socket
        .connect(SocketAddr::new(
            IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)),
            7080,
        ))
        .await
        .unwrap();

    loop {
        let new_state = update_receiver.recv().await;

        if let Some(new_state) = new_state {
            let msg = serde_json::to_string(&RadioState::from_state(&new_state)).unwrap();

            socket.send(&msg.as_bytes()).await.unwrap();
        }
    }
}
