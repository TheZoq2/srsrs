use std::collections::VecDeque;

pub struct FirFilter {
    coefficients: Vec<i16>,
    samples: VecDeque<i16>,
}

impl FirFilter {
    pub fn new(coefficients: Vec<i16>) -> Self {
        let samples = vec![0; coefficients.len()].into();
        Self {
            coefficients,
            samples,
        }
    }

    pub fn new_radio_filter() -> Self {
        let coefficients = vec![-4679, -6617, 4679, 13979, 4679, -6617, -4679];
        Self::new(coefficients)
    }

    pub fn push_sample(&mut self, sample: i16) -> i16 {
        self.samples.pop_back();
        self.samples.push_front(sample);

        let as_i32 = self
            .samples
            .iter()
            .zip(self.coefficients.iter())
            .map(|(c, x)| *c as i32 * *x as i32)
            .sum::<i32>()
            >> 16;
        as_i32 as i16
    }
}
