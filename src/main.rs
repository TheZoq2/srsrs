// This is needed because of the select statement in src/audio_effects.rs
// https://github.com/rust-lang/futures-rs/issues/1917
#![recursion_limit = "1024"]
#[macro_use]
extern crate log;

#[macro_use]
extern crate anyhow;

use std::net::ToSocketAddrs;
use std::str::FromStr;
use std::sync::Arc;

use audiopus::{coder::Decoder, Channels, SampleRate};
use cpal::{
    self,
    traits::{DeviceTrait, EventLoopTrait, HostTrait},
};
use flexi_logger::Logger;
use futures::channel::mpsc;
use futures::prelude::*;
use futures_util::stream::{SplitSink, SplitStream};
use rodio::{source::ChannelVolume, Sink};
use structopt::StructOpt;
use tokio;
use tokio::sync::Mutex;

use srs::{message::Coalition as SrsCoalition, Client, VoicePacket, VoiceStream};

mod audio_effects;
mod channel_logger;
mod dcs_control;
mod filter;
mod game_ui;
mod playback_info;
mod state;
mod ui;

use playback_info::PlaybackInfo;

use crate::audio_effects::EffectPlayer;

#[derive(StructOpt)]
enum Coalition {
    Blue,
    Red,
    Spectator,
}

impl From<Coalition> for SrsCoalition {
    fn from(other: Coalition) -> Self {
        match other {
            Coalition::Blue => SrsCoalition::Blue,
            Coalition::Red => SrsCoalition::Red,
            Coalition::Spectator => SrsCoalition::Spectator,
        }
    }
}

impl FromStr for Coalition {
    type Err = &'static str;
    fn from_str(coalition: &str) -> Result<Self, Self::Err> {
        match coalition {
            "red" | "r" => Ok(Coalition::Red),
            "blue" | "b" => Ok(Coalition::Blue),
            "spectator" | "s" => Ok(Coalition::Spectator),
            _ => Err("Could not coalition. Must be 'red' or 'blue'"),
        }
    }
}

// Distribute the same message to many streams
async fn split_channel<T: Clone>(
    mut rx: mpsc::Receiver<T>,
    mut tx1: mpsc::UnboundedSender<T>,
    mut tx2: mpsc::UnboundedSender<T>,
) -> Result<(), anyhow::Error> {
    loop {
        let received = rx.next().await.ok_or(anyhow!("Sender disconnected"))?;
        tx1.send(received.clone()).await?;
        tx2.send(received.clone()).await?;
    }
}

#[derive(StructOpt)]
#[structopt(name = "srsrs", about = "Unofficial srs client")]
enum Opt {
    Run {
        #[structopt()]
        server: String,
        #[structopt(short, long, default_value = "5002")]
        port: String,

        #[structopt()]
        coalition: Coalition,

        #[structopt(short, long)]
        username: Option<String>,
    },
    Configure,
}

#[tokio::main]
pub async fn main() -> anyhow::Result<()> {
    let (log_tx, log_rx) = std::sync::mpsc::channel();
    let log_writer = channel_logger::ChannelLogWriter::new(log::LevelFilter::Off, log_tx);
    Logger::with_env_or_str("srsrs")
        .log_target(flexi_logger::LogTarget::Writer(Box::new(log_writer)))
        .print_message()
        .start()
        .map_err(|e| anyhow!("Failed to initialise logger {}", e))?;

    let opt = Opt::from_args();

    match opt {
        Opt::Configure => {
            ui::input::do_remapping()?;
            return Ok(());
        }
        Opt::Run {
            server,
            port,
            coalition,
            username: cli_username,
        } => {
            let (game_tx, mut split_rx) = mpsc::channel(20);
            tokio::spawn(dcs_control::dcs_control(game_tx));

            // Start up the game message thread and wait for a username if it wasn't
            // specified on the CLI
            let username = match cli_username {
                Some(name) => name,
                None => {
                    println!("Waiting for a game message to get the username");
                    println!("Use the -u flag to force a username and start directly");
                    let game_msg = split_rx
                        .next()
                        .await
                        .expect("game message receiver disconnected before sending a username");
                    game_msg.name
                }
            };

            let local_set = tokio::task::LocalSet::new();

            let (shutdown_tx, shutdown_rx) = tokio::sync::oneshot::channel();

            let state = Arc::new(Mutex::new(state::StateManager::new()));

            let frequency = 245000000;
            let client = Client::new(&username, frequency, coalition.into());
            let sguid = client.sguid().to_string();

            let (split_tx1, game_rx) = mpsc::unbounded();
            let (split_tx2, radio_rx) = mpsc::unbounded();

            let mut addr = format!("{}:{}", server, port).to_socket_addrs().unwrap();

            let (sink, stream) = client
                .start(addr.next().unwrap(), Some(game_rx), shutdown_rx)
                .await?
                .split();

            tokio::spawn(game_ui::run(state.clone()));
            tokio::spawn(recv_voice_packets(stream, state.clone()));
            tokio::spawn(split_channel(split_rx, split_tx1, split_tx2));
            tokio::spawn(state::state_updater(state.clone(), radio_rx));
            local_set.spawn_local(audio_broadcast(sink, state.clone(), sguid));
            local_set.spawn_local(ui::run_ui(state.clone(), shutdown_tx, log_rx));
            local_set.await;

            Ok(())
        }
    }
}

/// Data associated with a single client which is currently transmitted
struct RadioReceiverBundle {
    pub id: [u8; 22],
    pub decoder: Decoder,
    pub sink: rodio::Sink,
    pub filter: filter::FirFilter,
}

async fn recv_voice_packets(
    mut stream: SplitStream<VoiceStream>,
    state: Arc<Mutex<state::StateManager>>,
) -> Result<(), anyhow::Error> {
    let mut output = [0i16; 1024];

    let device = rodio::default_output_device().unwrap();

    // let mut sinks = HashMap::new();
    let mut sinks = vec![];

    let (mut message_tx, message_rx) = mpsc::unbounded();
    tokio::spawn(EffectPlayer::new().run(message_rx));

    loop {
        match stream.next().await {
            Some(Ok(packet)) => {
                let mut sink_index = None;
                for (i, RadioReceiverBundle { id, .. }) in sinks.iter().enumerate() {
                    if id == &packet.client_sguid {
                        sink_index = Some(i);
                    }
                }
                if sink_index == None {
                    let dec = Decoder::new(SampleRate::Hz16000, Channels::Mono)
                        .expect("Failed to create decoder");
                    sinks.push(RadioReceiverBundle {
                        id: packet.client_sguid,
                        decoder: dec,
                        sink: Sink::new(&device),
                        filter: filter::FirFilter::new_radio_filter(),
                    });
                }
                let sink_index = sink_index.unwrap_or(sinks.len() - 1);
                let RadioReceiverBundle {
                    decoder: dec,
                    sink,
                    filter,
                    ..
                } = &mut sinks[sink_index];

                let decode_result = dec.decode(Some(&packet.audio_part), &mut output[..], false);

                match decode_result {
                    Ok(len) => {
                        let state = state.lock().await;

                        // If this packet should be played at all
                        if let Some(pb) = PlaybackInfo::get(&packet, &state.state().radios) {
                            sink.set_volume(pb.volume);

                            let filtered_samples = output[0..len]
                                .iter()
                                .map(|s| filter.push_sample(*s))
                                .map(|s| s.saturating_mul(6))
                                .collect::<Vec<_>>();

                            let source =
                                rodio::buffer::SamplesBuffer::new(1, 16000, filtered_samples);
                            let with_channel = ChannelVolume::new(source, pb.channel.clone());
                            sink.append(with_channel);

                            message_tx
                                .send((packet.unit_id, pb.channel.clone()))
                                .await
                                .ok();
                        }
                    }
                    Err(e) => error!("Decoder error: {:?}", e),
                }
            }
            Some(Err(e)) => {
                error!("Voice packet error {}", e);
            }
            None => Err(anyhow!("Voice packet sender disconnected"))?,
        }
    }
}

async fn audio_broadcast(
    mut sink: SplitSink<VoiceStream, VoicePacket>,
    state: Arc<Mutex<state::StateManager>>,
    sguid_str: String,
) -> Result<(), anyhow::Error> {
    let mut sguid = [0; 22];
    for i in 0..22 {
        sguid[i] = sguid_str.as_bytes()[i];
    }

    let encoder = audiopus::coder::Encoder::new(
        SampleRate::Hz16000,
        Channels::Mono,
        audiopus::Application::Voip,
    )
    .expect("Failed to create encoder");

    let host = cpal::default_host();
    let device = host.default_input_device().expect("Failed to get device");
    let mut format = device
        .default_input_format()
        .expect("Failed to get input format");
    format.sample_rate = cpal::SampleRate(16000);
    format.channels = 1;

    let event_loop = host.event_loop();
    let stream_id = event_loop
        .build_input_stream(&device, &format)
        .expect("failed to build input stream");
    event_loop
        .play_stream(stream_id)
        .expect("Failed to play input stream");

    let (tx, mut rx) = mpsc::unbounded();

    let rodio_device = rodio::default_output_device().unwrap();
    let debug_sink = Sink::new(&rodio_device);
    debug_sink.set_volume(1.0);

    tokio::spawn(async move {
        event_loop.run(move |id, event| {
            let data = match event {
                Ok(data) => data,
                Err(err) => panic!("Input stream encountered error: {:?}: {}", id, err),
            };
            match data {
                cpal::StreamData::Input {
                    buffer: cpal::UnknownTypeInputBuffer::U16(buffer),
                } => {
                    for sample in buffer.iter() {
                        let sample = cpal::Sample::to_i16(sample);
                        tx.unbounded_send(sample).unwrap();
                    }
                }
                cpal::StreamData::Input {
                    buffer: cpal::UnknownTypeInputBuffer::I16(buffer),
                } => {
                    for &sample in buffer.iter() {
                        tx.unbounded_send(sample).unwrap();
                    }
                }
                cpal::StreamData::Input {
                    buffer: cpal::UnknownTypeInputBuffer::F32(buffer),
                } => {
                    for &sample in buffer.iter() {
                        let sample = cpal::Sample::to_i16(&sample);
                        tx.unbounded_send(sample).unwrap();
                    }
                }
                _ => {}
            }
        });
    });

    let mut buffer = vec![];
    let mut output = [0; 16384];
    let mut packet_id = 0;
    loop {
        match rx.next().await {
            Some(sample) => {
                let (target_freq, unit_id) = {
                    let s = state.lock().await;
                    (s.should_transmit_where(), s.state().unit_id)
                };
                if let Some(frequency) = target_freq {
                    buffer.push(sample);

                    // 160 samples because the frame must be exactly
                    // 2.5, 5, 10, 20, 40 or 60 ms of audio data
                    // https://www.opus-codec.org/docs/html_api/group__opusencoder.html
                    const MONO_40MS: usize = 16000 /* * 1 channel */ * 40 / 1000;
                    if buffer.len() >= MONO_40MS {
                        // let source = rodio::buffer::SamplesBuffer::new(1, 16000, &buffer[0..MONO_20MS]);
                        // debug_sink.append(source);
                        // let with_channel = ChannelVolume::new(source, pb.channel);
                        let encode_result = encoder
                            .encode(&buffer.drain(0..MONO_40MS).collect::<Vec<_>>(), &mut output);
                        match encode_result {
                            Ok(len) => {
                                let mut audio_part = vec![];
                                audio_part.extend_from_slice(&output[0..len]);

                                let packet = VoicePacket {
                                    audio_part,
                                    frequencies: vec![frequency],
                                    unit_id,
                                    client_sguid: sguid,
                                    packet_id,
                                    hop_count: 0,
                                    transmission_sguid: sguid,
                                };
                                sink.send(packet)
                                    .await
                                    .expect("Failed to send voice packet");
                                packet_id = packet_id.wrapping_add(1);
                            }
                            Err(e) => warn!("Encoding failure: {:?}", e),
                        }
                    }
                }
            }
            None => {}
        }
    }
}
