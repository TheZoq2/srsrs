use std::{
    collections::HashMap,
    path::Path,
    time::{Duration, SystemTime},
};

use futures::{channel::mpsc::UnboundedReceiver, select, StreamExt};
use rodio::source::ChannelVolume;
use tokio::time;

/// Tries to read a wav file. If reading fails, warn and return an empty vector
fn read_wav(filename: impl AsRef<Path>) -> Vec<i16> {
    let filename = filename.as_ref();
    hound::WavReader::open(filename)
        .and_then(|mut reader| reader.samples::<i16>().collect::<hound::Result<Vec<_>>>())
        .map_err(|e| log::warn!("Failed to find {:?}. {}", filename, e))
        .unwrap_or_else(|_| vec![])
}

pub struct EffectSamples {
    pub receive_start: Vec<i16>,
    pub receive_end: Vec<i16>,
    pub uhf_noise: Vec<i16>,
}

impl EffectSamples {
    pub fn new() -> Self {
        Self {
            receive_start: read_wav("audio_effects/RADIO_TRANS_START.wav"),
            receive_end: read_wav("audio_effects/RADIO_TRANS_END.wav"),
            uhf_noise: read_wav("audio_effects/UHF_NOISE.wav"),
        }
    }

    pub fn play<'a, F>(&'a self, sink: &mut rodio::Sink, effect: F, channel: Vec<f32>)
    where
        F: Fn(&'a Self) -> &'a [i16],
    {
        let source = rodio::buffer::SamplesBuffer::new(1, 44100, effect(self));
        let with_channel = ChannelVolume::new(source, channel.clone());
        sink.append(with_channel)
    }
}

pub struct ClientInfo {
    pub last_seen: std::time::Duration,
    pub on_channel: Vec<f32>,
}

pub struct EffectPlayer {
    samples: EffectSamples,
    clients: HashMap<u32, ClientInfo>,
}

impl EffectPlayer {
    pub fn new() -> Self {
        Self {
            samples: EffectSamples::new(),
            clients: HashMap::new(),
        }
    }

    pub async fn run(mut self, message_notifier: UnboundedReceiver<(u32, Vec<f32>)>) {
        let mut message_notifier = message_notifier.fuse();
        let start_time = SystemTime::now();

        let mut stop_check_interval = time::interval(Duration::from_millis(10)).fuse();

        let device = rodio::default_output_device().unwrap();
        let mut effect_sink = rodio::Sink::new(&device);
        let noise_sinks = vec![rodio::Sink::new(&device), rodio::Sink::new(&device)];

        loop {
            select! {
                msg = message_notifier.next() => {
                    if let Some((unit_id, channels)) = msg {
                        // Check if this is an unseen client
                        if let Some(prev) = self.clients.get_mut(&unit_id) {
                            if let Ok(time) = start_time.elapsed() {
                                prev.last_seen = time;
                                prev.on_channel = channels;
                            }
                        }
                        else {
                            if let Ok(time) = start_time.elapsed() {
                                self.clients.insert(
                                    unit_id,
                                    ClientInfo{last_seen: time, on_channel: channels.clone()}
                                );
                                self.samples.play(&mut effect_sink, |e| &e.receive_start, channels);
                            }
                        }
                    }
                },
                _ = stop_check_interval.next() => {
                    // Clean up our transmitters
                    if let Ok(current_time) = start_time.elapsed() {
                        let mut end_channel = None;
                        self.clients.retain(|_, info| {
                            // 70 is fairly arbitrary, but should be enough since opus
                            // allows at most 60 ms packet size
                            if (current_time - info.last_seen).as_millis() < 150 {
                                true
                            }
                            else {
                                end_channel = Some(info.on_channel.clone());
                                false
                            }
                        });

                        if let Some(channel) = end_channel {
                            self.samples.play(&mut effect_sink, |e| &e.receive_end, channel);
                        }
                    };

                    // Compute the volume of the noise to be played
                    let mut channel_volumes = vec![0., 0.];
                    for (_, info) in &self.clients {
                        for (volume, channel) in channel_volumes.iter_mut().zip(info.on_channel.iter()) {
                            *volume += channel;
                        }
                    }

                    // Set the volume and queue up more audio if needed
                    for ((i, sink), volume) in noise_sinks.iter().enumerate().zip(channel_volumes.iter()) {
                        const NOISE_VOLUME: f32 = 0.1;
                        sink.set_volume(*volume * NOISE_VOLUME);

                        // Noise is a pre-recorded sample, to never run out of noise to play,
                        // we'll check if the number of items in the queue is < 2, and if so,
                        // add a new copy
                        if sink.len() < 2 {
                            let source = rodio::buffer::SamplesBuffer::new(
                                1,
                                44100,
                                self.samples.uhf_noise.clone()
                            );
                            let mut channel = noise_sinks.iter().map(|_| 0.).collect::<Vec<_>>();
                            channel[i] = 1.0;
                            let with_channel = ChannelVolume::new(source, channel);
                            sink.append(with_channel)
                        }
                    }
                }
            }
        }
    }
}
