use std::sync::Arc;

use futures::channel::mpsc::UnboundedReceiver;
use futures::StreamExt;
use tokio::sync::{watch, Mutex};

use srs::message::{GameMessage, Modulation, Radio};

#[derive(Clone)]
pub struct State {
    pub radios: Vec<Radio>,
    pub ptt_active: bool,
    pub radio_index: usize,
    pub unit_id: u32,
}

impl State {
    /// Returns the frequency which is selected for transmitting
    pub fn active_frequency(&self) -> Option<srs::Frequency> {
        self.selected_radio().map(|(_, radio)| {
            let modulation = match radio.modulation {
                srs::message::Modulation::AM => srs::Modulation::AM,
                srs::message::Modulation::FM => srs::Modulation::FM,
                srs::message::Modulation::Intercom => srs::Modulation::Intercom,
                srs::message::Modulation::Disabled => srs::Modulation::Disabled,
                srs::message::Modulation::HaveQuick => srs::Modulation::AM,
                srs::message::Modulation::Satcom => srs::Modulation::AM,
                srs::message::Modulation::Mids => srs::Modulation::AM,
            };

            // NOTE: To work when srs encryption is turned off in the server we aparently
            // need to send Encryption::None. I have no idea why, but if using the encryption
            // set by the game message, it becomes full and all srs users get static.
            let encryption = srs::Encryption::None;
            /*
            let encryption = match radio.enc_mode {
                srs::message::EncryptionMode::NoEncryption => srs::Encryption::None,
                srs::message::EncryptionMode::EncryptionJustOverlay => srs::Encryption::JustOverlay,
                srs::message::EncryptionMode::EncryptionFull => srs::Encryption::Full,
                srs::message::EncryptionMode::EncryptionCockpitToggleOverlayCode => {
                    srs::Encryption::CockpitToggleOverlayCode
                }
            };
            */

            srs::Frequency {
                freq: radio.freq,
                modulation,
                encryption,
            }
        })
    }

    pub fn selected_radio(&self) -> Option<(usize, &Radio)> {
        self.radios
            .iter()
            .skip(self.radio_index)
            .next()
            .map(|r| (self.radio_index, r))
    }

    /// Returns the frequency to transmit on if push to talk is held
    pub fn should_transmit_where(&self) -> Option<srs::Frequency> {
        if self.ptt_active {
            self.active_frequency()
        } else {
            None
        }
    }
}

/// Wrapper around state that only provides mutable access to the state via
/// the `modify` function which allows other modules to listen to the update_receiver
/// to find out about state updates
// NOTE: In theory, we should probably use one of mutex, or watch to manage the state being shared
// everywhere, but this allows things of less importance, like the game UI to look at the state
// without blocking the rest of the program
pub struct StateManager {
    inner: State,
    pub update_receiver: watch::Receiver<State>,
    update_sender: watch::Sender<State>,
}

impl StateManager {
    pub fn new() -> Self {
        let initial_state = State {
            radios: vec![],
            ptt_active: false,
            radio_index: 1,
            unit_id: 0,
        };

        let (update_sender, update_receiver) = watch::channel(initial_state.clone());

        Self {
            inner: initial_state,
            update_receiver,
            update_sender,
        }
    }

    /// Returns the frequency to transmit on if push to talk is held
    pub fn should_transmit_where(&self) -> Option<srs::Frequency> {
        self.inner.should_transmit_where()
    }

    pub fn state(&self) -> &State {
        &self.inner
    }

    pub fn modify<F>(&mut self, f: F)
    where
        F: Fn(&mut State),
    {
        f(&mut self.inner);
        let _ = self.update_sender.broadcast(self.inner.clone());
    }

    pub fn set_ptt(&mut self, ptt_active: bool) {
        self.modify(|s| s.ptt_active = ptt_active)
    }

    pub fn prev_radio(&mut self) {
        self.modify(|s| {
            s.radio_index = s.radio_index.saturating_sub(1);
        })
    }

    pub fn next_radio(&mut self) {
        self.modify(|s| {
            s.radio_index += 1 as usize;
        })
    }

    pub fn radio_add_mhz(&mut self, amount: f64) {
        self.modify(|s| {
            let radio_index = s.radio_index;

            match s.radios[radio_index].modulation {
                Modulation::AM => {
                    if s.radios[radio_index].freq + (amount * 1000000.0) <= 400000000.0
                        && s.radios[radio_index].freq + (amount * 1000000.0) >= 1000000.0
                    {
                        s.radios[radio_index].freq += amount * 1000000.0;
                    }
                }
                Modulation::FM => {
                    if s.radios[radio_index].freq + (amount * 1000000.0) <= 76000000.0
                        && s.radios[radio_index].freq + (amount * 1000000.0) >= 1000000.0
                    {
                        s.radios[radio_index].freq += amount * 1000000.0;
                    }
                }
                _ => {}
            }
        })
    }
}

pub async fn state_updater(
    state: Arc<Mutex<StateManager>>,
    mut message_rx: UnboundedReceiver<GameMessage>,
) {
    while let Some(msg) = message_rx.next().await {
        let mut s = state.lock().await;

        s.modify(|state| {
            state.radios = msg.radios.clone();
            state.unit_id = msg.unit_id.clone();
        });
    }
}
