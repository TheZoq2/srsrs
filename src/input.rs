use std::fs::File;
use std::io::{self, prelude::*};
use std::path::PathBuf;
use std::sync::Arc;

use anyhow::{Context, Result};

use gilrs::{Event, EventType, Gilrs};

use serde::{Deserialize, Serialize};

use tokio::sync::Mutex;
use x11rb::connection::Connection;
use x11rb::protocol::xproto::grab_key;
use x11rb::protocol::xproto::*;
use x11rb::protocol::Event as X11Event;
use x11rb::rust_connection::RustConnection;
use x11rb::COPY_DEPTH_FROM_PARENT;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("No binding config file exists")]
    NoConfigFile,
    // This is a string because anyhow doesn't like gilrs errors aparently
    #[error("GILRS error: {0}")]
    GilrsError(String),
    #[error("IO error {0}")]
    Io(#[from] std::io::Error),
    #[error("Toml deserialization error {0}")]
    Json(#[from] serde_json::Error),
    #[error("Failed to connect to x server: {0}")]
    X11ConnectError(#[from] x11rb::errors::ConnectError),
}

#[derive(Clone, Serialize, Deserialize)]
pub enum Key {
    // NOTE: We may want to use uuid here
    Gamepad(String, u32),
    // x11 modifier state, X11 keycode
    Keyboard(u8, u16),
}

impl Key {
    pub fn check_gilrs(&self, gamepad: String, code: &gilrs::ev::Code) -> bool {
        match self {
            Key::Gamepad(g, c) if g == &gamepad && *c == code.into_u32() => true,
            _ => false,
        }
    }
    #[allow(dead_code)]
    pub fn check_x11(&self, key: u8, status: u16) -> bool {
        match self {
            Key::Keyboard(k, s) if k == &key && *s == status & MODIFIER_KEY_MASK => true,
            _ => false,
        }
    }
}

const MODIFIER_KEY_MASK: u16 = KeyButMask::Shift as u16
    | KeyButMask::Lock as u16
    | KeyButMask::Control as u16
    | KeyButMask::Mod1 as u16
    | KeyButMask::Mod2 as u16
    | KeyButMask::Mod3 as u16
    | KeyButMask::Mod4 as u16
    | KeyButMask::Mod5 as u16;

#[derive(Clone, Default, Serialize, Deserialize)]
struct Bindings {
    pub ptt: Option<Key>,
    pub next_radio: Option<Key>,
    pub prev_radio: Option<Key>,
}

impl Bindings {
    pub fn filename() -> PathBuf {
        "srsrs.json".into()
    }
    pub fn from_file() -> std::result::Result<Self, Error> {
        if !Self::filename().exists() {
            Err(Error::NoConfigFile)
        } else {
            let mut file = File::open(&Self::filename())?;
            let mut content = String::new();
            file.read_to_string(&mut content)?;

            Ok(serde_json::from_str::<Self>(&content)?)
        }
    }
    pub fn save(&self) -> std::result::Result<(), Error> {
        let content = serde_json::to_string(self)?;
        let mut file = File::create(&Self::filename())?;
        file.write_all(&mut content.as_bytes())?;
        Ok(())
    }
}

pub struct InputState {
    pub ptt_active: Mutex<bool>,
    pub radio_index: Mutex<i32>,
}

impl InputState {
    pub fn new() -> Self {
        Self {
            ptt_active: Mutex::new(false),
            radio_index: Mutex::new(0),
        }
    }
}

pub fn run_input_tasks(local_set: &tokio::task::LocalSet) -> Result<Arc<InputState>> {
    let state = Arc::new(InputState::new());
    let bindings = Bindings::from_file()?;

    let state_ = state.clone();
    let bindings_ = bindings.clone();

    tokio::task::spawn(async move { run_x11_task(state_, bindings_).await });

    let state_ = state.clone();
    local_set.spawn_local(async move { run_gilrs_task(state_, bindings).await });

    Ok(state)
}

/**
  Runs a taks for reading gamepad input
*/
async fn run_gilrs_task(input: Arc<InputState>, bindings: Bindings) -> ! {
    info!("Gamepad thread starting");
    let mut gilrs = Gilrs::new()
        .map_err(|e| Error::GilrsError(format!("{}", e)))
        .expect("Failed to initialise gilrs");

    macro_rules! check_key {
        ($key:ident, $gamepad:expr, $code:expr) => {
            bindings
                .$key
                .as_ref()
                .map(|x| x.check_gilrs($gamepad, $code))
                .unwrap_or(false)
        };
    }

    loop {
        while let Some(Event { id, event, .. }) = &gilrs.next_event() {
            if let EventType::ButtonPressed(_, code) = event {
                let gamepad = gilrs.gamepad(id.clone()).name().to_string();
                if check_key!(ptt, gamepad, code) {
                    *input.ptt_active.lock().await = true
                }
            }
            if let EventType::ButtonReleased(_, code) = event {
                let gamepad = gilrs.gamepad(id.clone()).name().to_string();
                if check_key!(ptt, gamepad, code) {
                    *input.ptt_active.lock().await = false
                }
            }
        }
        tokio::task::yield_now().await
    }
}

#[allow(unused_variables)]
async fn run_x11_task(input: Arc<InputState>, bindings: Bindings) {
    info!("Starting X11 key receiver");
    let (x11, screen_num) = RustConnection::connect(None).expect("failed to open x11 connection");
    let screen = &x11.setup().roots[screen_num];

    let grab_key_result = x11
        .grab_key(
            false,
            screen.root,
            KeyButMask::Shift as u16,
            65,
            GrabMode::Async,
            GrabMode::Async,
        )
        .map_err(|e| error!("Failed to grab x11 keys {}", e))
        .and_then(|c| {
            c.check()
                .map_err(|e| error!("Failed to grab x11 keys {}", e))
        });

    if grab_key_result.is_err() {
        err!("Failed to grab x11 input");
        return;
    }

    info!("Starting x11 input task");
    loop {
        tokio::task::yield_now().await;

        while let Some(event) = x11.poll_for_event().expect("failed to poll for x11 events") {
            match event {
                X11Event::KeyPress(KeyPressEvent { detail, state, .. }) => {
                    info!("got a key press event");
                }
                _ => {}
            }
            info!("saw an event");
        }
    }
}

